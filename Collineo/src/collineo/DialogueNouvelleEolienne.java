package collineo;

import java.awt.Dimension;
import java.awt.Frame;

import java.awt.Rectangle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class DialogueNouvelleEolienne extends JDialog {
    private JLabel jLabel1 = new JLabel();
    private JLabel jLabel2 = new JLabel();
    private JLabel jLabel3 = new JLabel();
    private JLabel jLabel4 = new JLabel();
    private JLabel jLabel5 = new JLabel();
    private JTextField champNomEolienne = new JTextField();
    private JTextField champModele = new JTextField();
    private JTextField champLargeur = new JTextField();
    private JTextField champHauteur = new JTextField();
    private JButton boutonOk = new JButton();
    private JButton boutonAnnuler = new JButton();
    private String nomEolienne;
    private String modele;
    private int largeur;
    private int hauteur;
    private ArrayList tab;
    private FrameEdit parent;

    public DialogueNouvelleEolienne(FrameEdit parent, String title, boolean modal) {
        super(parent, title, modal);
        this.parent=parent;
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.setSize( new Dimension( 400, 300 ) );
        this.getContentPane().setLayout( null );
        this.setTitle("Nouvelle �olienne");
        jLabel1.setText("jLabel1");
        jLabel2.setText("Nom de l'�olienne");
        jLabel2.setBounds(new Rectangle(30, 20, 105, 25));
        jLabel3.setText("Mod�le");
        jLabel3.setBounds(new Rectangle(30, 55, 105, 25));
        jLabel4.setText("Largeur");
        jLabel4.setBounds(new Rectangle(30, 95, 105, 25));
        jLabel5.setText("Hauteur");
        jLabel5.setBounds(new Rectangle(30, 135, 105, 25));
        champNomEolienne.setBounds(new Rectangle(140, 20, 145, 25));
        champModele.setBounds(new Rectangle(140, 55, 145, 25));
        champLargeur.setBounds(new Rectangle(140, 95, 145, 25));
        champHauteur.setBounds(new Rectangle(140, 130, 145, 25));
        boutonOk.setText("Ok");
        boutonOk.setBounds(new Rectangle(80, 200, 100, 30));
        boutonAnnuler.setText("Annuler");
        boutonAnnuler.setBounds(new Rectangle(200, 200, 100, 30));
        this.getContentPane().add(boutonAnnuler, null);
        this.getContentPane().add(boutonOk, null);
        this.getContentPane().add(champHauteur, null);
        this.getContentPane().add(champLargeur, null);
        this.getContentPane().add(champModele, null);
        this.getContentPane().add(champNomEolienne, null);
        this.getContentPane().add(jLabel5, null);
        this.getContentPane().add(jLabel4, null);
        this.getContentPane().add(jLabel3, null);
        this.getContentPane().add(jLabel2, null);
        
        Ecouteur ec = new Ecouteur();
        boutonOk.addActionListener(ec);
        boutonAnnuler.addActionListener(ec);
    }
    
    private class Ecouteur implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            
            if(e.getSource() == boutonOk){
                try{
                    nomEolienne = champNomEolienne.getText();
                    modele = champModele.getText();
                    largeur = Integer.parseInt(champLargeur.getText())  ;
                    hauteur = Integer.parseInt(champHauteur.getText())   ;
                    if(nomEolienne.equals("") || modele.equals("") ||  champLargeur.equals("") || champHauteur.equals("") ){
                        JOptionPane.showMessageDialog(DialogueNouvelleEolienne.this, "Aucun champs vide accept�");
                    }
                    else{
                        parent.recupererInfosDialogueEolienne(nomEolienne, modele, largeur, hauteur);
                        
                        setVisible(false);
                        dispose();
                    }
                    
                    
                }
                catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(DialogueNouvelleEolienne.this, "La largeur et la hauteur doivent �tre un nombre");
                }
                
                
            }
            
            else if (e.getSource() == boutonAnnuler){
                dispose();
                
            }
                
            
        }
        
        
    }
    
    
}
