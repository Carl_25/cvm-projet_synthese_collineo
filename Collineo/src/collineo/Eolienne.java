package collineo;

import java.awt.Point;

import java.io.Serializable;

import java.util.Vector;

public class Eolienne implements Serializable {
    private static int id=0;
    private String nomEolienne;
    private String modele;
    private int posX;
    private int posY;
    //private double orientationPointsCardinaux;
    //private double orientationCheminDacces;
    private int largeur;
    private int hauteur;
    
    public Eolienne(String nomEolienne, String modele, int posX, int posY, int largeur, int hauteur){
        this.id += 1;
        this.nomEolienne=nomEolienne;
        this.modele=modele;
        this.posX=posX;
        this.posY=posY;
        this.largeur=largeur;
        this.hauteur=hauteur;
    }


    public static void setId(int id) {
        Eolienne.id = id;
    }

    public static int getId() {
        return id;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public String getNomEolienne() {
        return nomEolienne;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public int getLargeur() {
        return largeur;
    }

    public int getHauteur() {
        return hauteur;
    }


    public void setNomEolienne(String nomEolienne) {
        this.nomEolienne = nomEolienne;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }

    public void setHauteur(int hauteur) {
        this.hauteur = hauteur;
    }

}
