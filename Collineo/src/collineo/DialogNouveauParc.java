package collineo;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;

import java.awt.Image;
import java.awt.Rectangle;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.image.BufferedImage;

import java.io.File;

import java.io.IOException;

import javax.imageio.ImageIO;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class DialogNouveauParc extends JDialog {
    private JLabel jLabel1 = new JLabel();
    private JLabel jLabel2 = new JLabel();
    private JLabel jLabel4 = new JLabel();
    private JTextField champNomParc = new JTextField();
    private JTextField champNomOperateur = new JTextField();
    private JButton boutonParcourir = new JButton();
    private JFileChooser fc = new JFileChooser("images");
    private File file=null;
    private BufferedImage image;
    private JButton boutonOk = new JButton();
    private FrameEdit parent;
    private JButton boutonOk1 = new JButton();
    private JButton boutonOk2 = new JButton();
    private JButton boutonOk3 = new JButton();
    private JButton boutonAnnuler = new JButton();
    private String cheminFichier;

    public DialogNouveauParc(FrameEdit parent, String title, boolean modal) {
        super(parent, title, modal);
        this.parent=parent;
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.setSize(new Dimension(370, 224));
        this.getContentPane().setLayout( null );
        this.setTitle("Nouveau Parc");
        jLabel1.setText("Nom du parc : ");
        jLabel1.setBounds(new Rectangle(10, 20, 100, 20));
        jLabel2.setText("Choisir une map :");
        jLabel2.setBounds(new Rectangle(10, 90, 140, 20));
        jLabel4.setText("Nom de l'opérateur :");
        jLabel4.setBounds(new Rectangle(10, 55, 115, 20));
        champNomParc.setBounds(new Rectangle(150, 20, 120, 20));
        champNomOperateur.setBounds(new Rectangle(150, 55, 120, 20));
        boutonParcourir.setText("Parcourir...");
        boutonParcourir.setBounds(new Rectangle(150, 90, 120, 20));
        this.getContentPane().add(boutonAnnuler, null);
        this.getContentPane().add(boutonOk, null);
        this.getContentPane().add(boutonParcourir, null);
        this.getContentPane().add(champNomOperateur, null);
        this.getContentPane().add(champNomParc, null);
        this.getContentPane().add(jLabel4, null);


        this.getContentPane().add(jLabel2, null);
        this.getContentPane().add(jLabel1, null);
        Ecouteur ec = new Ecouteur();
        boutonParcourir.addActionListener(ec);
        boutonAnnuler.addActionListener(ec);
        boutonOk.addActionListener(ec);
        boutonOk.setText("Ok");
        boutonOk.setBounds(new Rectangle(80, 135, 100, 20));
        boutonOk1.setText("Ok");
        boutonOk1.setBounds(new Rectangle(15, 140, 75, 21));
        boutonOk2.setText("Ok");
        boutonOk2.setBounds(new Rectangle(85, 135, 75, 21));
        boutonOk3.setText("Ok");
        boutonOk3.setBounds(new Rectangle(25, 135, 75, 21));
        boutonAnnuler.setText("Annuler");
        boutonAnnuler.setBounds(new Rectangle(195, 135, 105, 20));
    }

    private class Ecouteur implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == boutonParcourir){
                int returnVal = fc.showOpenDialog(DialogNouveauParc.this);
                file = null;
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    file = fc.getSelectedFile();
                    cheminFichier = file.getPath();
                    
                    image = null;
                    try {
                        image = ImageIO.read(file);
                        
                    } catch (IOException exc) {
                        exc.printStackTrace();
                    }  
                } 
                else {
                    //User did not choose a valid file
                }
                
                
            }
            
            if(e.getSource() == boutonOk){
                try{
                    parent.recupererInfosDialogueNouveauParc(champNomParc.getText(), champNomOperateur.getText(), image,cheminFichier );
                    dispose();
                    
                    //rendre accessible le bouton enregistrer
                    parent.getBoutonEnregistrer().setEnabled(true);
                }
                catch(NullPointerException npe){
                    JOptionPane.showMessageDialog(DialogNouveauParc.this, "Entrez des informations pertinentes.");
                }
                
            }
            if(e.getSource() == boutonAnnuler){
                dispose();
            }
            
            
        }
    }
    

}
