package collineo;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;

import org.w3c.dom.Document;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.awt.event.MouseMotionListener;

import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import javax.imageio.ImageIO;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import javax.swing.JTextField;
import javax.swing.JToggleButton;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;

import javax.xml.transform.TransformerFactory;

import javax.xml.transform.dom.DOMSource;

import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FrameEdit extends JFrame {
    private int xEolienne=-30;
    private int yEolienne=-30;
    private Vector<Orientation> listeOrientation = new Vector<Orientation>();
    private Hashtable<String,Parc> listeParcs;
    private Vector<Eolienne> listeEoliennes;
    private Vector<CheminAcces> listeChemins ;
    private Hashtable<String,CheminAcces> h = new Hashtable<String,CheminAcces>();
    private JMenuBar jMenuBar1 = new JMenuBar();
    private JMenu jMenu1 = new JMenu();
    private JMenuItem menuNouveauParc = new JMenuItem();
    private SurfaceDessin surface; 
    private JLabel jLabel1 = new JLabel();
    private JToggleButton boutonAjouterEolienne = new JToggleButton();
    private ButtonGroup buttonGroupe = new ButtonGroup();
    private Ecouteur ec;
    private EcouteurSurface ecSurface;
    private DialogNouveauParc d;
    private boolean scrollImage = true;
    private String pathImage;
    private Orientation orientation;
    private int largeurImage;
    private int hauteurImage;
    private int x1chemin;
    private int y1chemin;
    private int x2chemin;
    private int y2chemin;
    private int transX,transY;
    private int x1orientation;
    private int y1orientation;
    private int x2orientation;
    private int y2orientation;
    private int xImageDepart, yImageDepart;
    private int xImage,yImage;
    private float zoom=1f;
    private DialogueNouvelleEolienne dialogueEolienne;
    private String nomEolienne;
    private String modeleEolienne;
    private int largeurEolienne = 12;
    private int hauteurEolienne = 12;
    private String nomParc;
    private String nomOperateur;
    private JTextField champOperateur = new JTextField();
    private BufferedImage image;
    private JToggleButton boutonOrientation = new JToggleButton();
    private JButton boutonEnregistrer = new JButton();
    private File fichierBD = new File("BD.xml");
    DocumentBuilderFactory docFactory;
    DocumentBuilder docBuilder;
    Element collineo;
    Parc parc;
    private JPanel panelInfosEolienne = new JPanel();
    private GridLayout gridLayout1 = new GridLayout();
    private JLabel jLabel3 = new JLabel();
    private JLabel jLabel4 = new JLabel();
    private JLabel jLabel5 = new JLabel();
    private JLabel jLabel6 = new JLabel();
    private JTextField champNomSelection = new JTextField();
    private JTextField champModele = new JTextField();
    private JTextField champHauteur = new JTextField();
    private JTextField champLargeur = new JTextField();
    private JLabel labelNomParc = new JLabel();
    private JComboBox comboParcs = new JComboBox();
    private JLabel labelImage = new JLabel();
    private JLabel jLabel2 = new JLabel();
    private JTextField champParcEnCours = new JTextField();
    private JToggleButton jToggleButton1 = new JToggleButton();
    private JToggleButton jToggleButton2 = new JToggleButton();
    private JToggleButton jToggleButton3 = new JToggleButton();
    Document doc;
    private boolean etatDessinerChemin = false;
    private JButton boutonEnregistrer1 = new JButton();
    private JButton boutonSupprimerParc = new JButton();
    private JToggleButton boutonSupprimerEol = new JToggleButton();


    public FrameEdit() {
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        listeParcs = new Hashtable<String,Parc>();
        this.getContentPane().setLayout( null );
        this.setSize(new Dimension(1100, 950));
        this.setJMenuBar(jMenuBar1);

        this.setBackground(Color.white);
        
        surface = new SurfaceDessin();
        this.getContentPane().setLayout(null );
        surface.setSize(750, 750); //d�finir la taille de la surface
        surface.setLocation(50, 50); //endroit o� elle est ajout�e
        this.getContentPane().add(boutonSupprimerEol, null);
        this.getContentPane().add(boutonSupprimerParc, null);
        this.getContentPane().add(champParcEnCours, null);
        this.getContentPane().add(jLabel2, null);
        this.getContentPane().add(surface);
        surface.setBackground(Color.black);
      
        jToggleButton3.setText("jToggleButton3");
        boutonEnregistrer1.setText("Enregistrer");
        boutonEnregistrer1.setBounds(new Rectangle(50, 810, 120, 30));
        boutonEnregistrer1.setEnabled(false);
        boutonSupprimerParc.setText("Supprimer ce parc");
        boutonSupprimerParc.setBounds(new Rectangle(645, 810, 155, 30));
        jToggleButton2.setText("jToggleButton2");
        jToggleButton1.setText("jToggleButton1");
        this.getContentPane().add(surface);
        
        panelInfosEolienne.add(jLabel3, null);
        panelInfosEolienne.add(champNomSelection, null);
        panelInfosEolienne.add(jLabel4, null);
        panelInfosEolienne.add(champModele, null);
        panelInfosEolienne.add(jLabel5, null);
        panelInfosEolienne.add(champLargeur, null);
        panelInfosEolienne.add(jLabel6, null);
        panelInfosEolienne.add(champHauteur, null);
        this.getContentPane().add(comboParcs, null);
        this.getContentPane().add(labelNomParc, null);
        this.getContentPane().add(panelInfosEolienne, BorderLayout.CENTER);
        this.getContentPane().add(boutonEnregistrer, null);

        this.getContentPane().add(boutonOrientation, null);

        this.getContentPane().add(champOperateur, null);
        this.getContentPane().add(boutonAjouterEolienne, null);
        this.getContentPane().add(jLabel1, null);
        jLabel1.setText("Op�rateur en action : ");
        jLabel1.setBounds(new Rectangle(815, 50, 140, 25));
        boutonAjouterEolienne.setText("Ajouter une �olienne");
        boutonAjouterEolienne.setBounds(new Rectangle(815, 85, 170, 25));
        jMenu1.setText("Fichier");
        menuNouveauParc.setText("Nouveau parc");
        jMenu1.add(menuNouveauParc);
        jMenuBar1.add(jMenu1);
        
        champNomSelection.setEditable(false);
        champModele.setEditable(false);
        champLargeur.setEditable(false);
        champHauteur.setEditable(false);


        buttonGroupe.add(boutonAjouterEolienne);
        buttonGroupe.add(boutonOrientation);
        buttonGroupe.add(boutonSupprimerEol);

        ec = new Ecouteur();
        ecSurface = new EcouteurSurface();
        surface.addMouseListener(ecSurface);
        surface.addMouseMotionListener(ecSurface);
        surface.addMouseWheelListener(ecSurface);
        
        
        menuNouveauParc.addActionListener(ec);
        boutonEnregistrer.addActionListener(ec);
        boutonSupprimerParc.addActionListener(ec);
        boutonSupprimerEol.addActionListener(ec);

        boutonSupprimerEol.setText("Supprimer une �olienne");
        boutonSupprimerEol.setBounds(new Rectangle(815, 120, 170, 25));
        champOperateur.setBounds(new Rectangle(945, 50, 105, 20));
        boutonOrientation.setText("Orientation de la carte");
        boutonOrientation.setBounds(new Rectangle(815, 155, 170, 25));
        boutonAjouterEolienne.addActionListener(ec);
        boutonOrientation.addActionListener(ec);
        boutonEnregistrer.setText("Enregistrer");
        boutonEnregistrer.setBounds(new Rectangle(50, 810, 120, 30));
        boutonEnregistrer.setEnabled(false);


        panelInfosEolienne.setBounds(new Rectangle(820, 200, 235, 140));
        panelInfosEolienne.setLayout(gridLayout1);
        gridLayout1.setColumns(2);
        gridLayout1.setRows(4);
        jLabel3.setText("Nom de l'�olienne : ");
        jLabel4.setText("Mod�le : ");
        jLabel5.setText("Largeur : ");
        jLabel6.setText("Hauteur : ");
        champNomSelection.setSize(new Dimension(50, 25));
        /*champNomSelection.setEditable(false);
        champModele.setEditable(false);
        champHauteur.setEditable(false);
        champLargeur.setEditable(false);*/

        labelNomParc.setBounds(new Rectangle(50, 20, 140, 25));
        comboParcs.setBounds(new Rectangle(50, 15, 120, 25));
        labelImage.setBackground(new Color(237, 237, 71));
        labelImage.setPreferredSize(new Dimension(600, 600));
        jLabel2.setText("Parc en cours : ");
        jLabel2.setBounds(new Rectangle(190, 15, 110, 25));
        champParcEnCours.setBounds(new Rectangle(295, 15, 125, 25));
        champParcEnCours.setEditable(false);
        comboParcs.addActionListener(ec);
        
      
                    
        if (fichierBD.exists() ){
            System.out.println("le fichier existe");
            ///////////////////////////////////////////////////////////////////
            //Charger le base de donn�es xml//////////////////////////////////
            ////////////////////////////////////////////////////////////
            chargerBd();
            loaderImage();
            
                        
        }
        else{ //la bd n'existe pas
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            doc = docBuilder.newDocument();
            collineo = doc.createElement("Collineo");
            doc.appendChild(collineo);
            
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(fichierBD );

            transformer.transform(source, result);
            System.out.println("Bd cr��!");
            x1orientation = y1orientation = x2orientation = y2orientation = -10000;
        }
                    
        
        

    }

    public class Ecouteur implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == boutonSupprimerParc){
                try{
                    buttonGroupe.clearSelection();
                    int reponse = JOptionPane.showConfirmDialog(FrameEdit.this, "Etes-vous certain de vouloir supprimer ce parc?");
                    if(JOptionPane.YES_OPTION == reponse){
                        // Supprimer tous les enfants du parcs
                        supprimerParc(doc.getFirstChild(), comboParcs.getSelectedItem().toString());
                        commit();
                        
                        //enlever le parc de la liste de parcs
                        listeParcs.remove(comboParcs.getSelectedItem().toString() );
                        //enlever le parc du comboBox
                        comboParcs.removeItem(comboParcs.getSelectedItem().toString() );
                        image = null;
                        xEolienne = yEolienne = -10000;
                        x1orientation = y1orientation = x2orientation = y2orientation = -10000;
                        x1chemin = y1chemin = x2chemin = y2chemin = -10000;
                        loaderImage();
                    }
                }
                catch(NullPointerException npe){
                    JOptionPane.showMessageDialog(FrameEdit.this, "Impossible de supprimer ce parc puisqu'il n'est pas encore enregistr�");
                }
                
                
            }
            
            else if (e.getSource() == menuNouveauParc){
                d = new DialogNouveauParc(FrameEdit.this, "Nouveau Parc", true);
                d.setLocationRelativeTo(FrameEdit.this);
                d.setVisible(true);
                listeEoliennes = new Vector<Eolienne>();
                listeChemins = new Vector<CheminAcces>();
                listeOrientation = new Vector<Orientation>();
                
                comboParcs.setSelectedItem(null);
                champParcEnCours.setText(nomParc);
                champOperateur.setText(nomOperateur);
                xEolienne = -30;
                yEolienne = -30;
                x1chemin = -1000;
                x2chemin = -1000;
                y1chemin = -1000;
                y2chemin = -1000;
                x1orientation = y1orientation = x2orientation = y2orientation = -10000;
                repaint();
                
            }
            else if (e.getSource() == comboParcs){
                if(comboParcs.getSelectedItem() != null && listeParcs.size() != 0){
                    if(listeParcs.get(comboParcs.getSelectedItem().toString() ) != null){
                        String operateur = listeParcs.get(comboParcs.getSelectedItem().toString() ).getNomOperateur();
                        champOperateur.setText(operateur);
                        zoom=1;
                        transX=0;
                        transY=0;
                        x1orientation = y1orientation = x2orientation = y2orientation = -10000;
                        xEolienne = -10000;
                        yEolienne = -10000;
                        x1chemin = y1chemin = x2chemin = y2chemin = -10000;
                        
                        String path = listeParcs.get(comboParcs.getSelectedItem().toString()).getPathImage();
                        System.out.println("Chemin image : "+path);
                        try {
                            image = ImageIO.read(new File(path));
                            
                        } catch (IOException exc) {
                            exc.printStackTrace();
                        } 
                    }
                    
                }
                
                
            }
            
            else if(e.getSource() ==  boutonAjouterEolienne){
                scrollImage = false;
                dialogueEolienne = new DialogueNouvelleEolienne(FrameEdit.this, "Nouvelle Eolienne", true);
                dialogueEolienne.setLocationRelativeTo(FrameEdit.this);
                dialogueEolienne.setVisible(true);
                
            }
            else if (e.getSource() == boutonOrientation){
                scrollImage = false;
            }
            else if (e.getSource() == boutonSupprimerEol){
                scrollImage = false;
            }
            
            else if(e.getSource() == boutonEnregistrer){
                ///////////////////////////////////////////////////////
                //ENREGISTRER DANS LA BASE DE DONN�ES//////////////////
                ///////////////////////////////////////////////////////
                try{
                    // Parc Eolien
                    Element parcEolien = doc.createElement("Parc_Eolien");
                    doc.getDocumentElement().appendChild(parcEolien);
                    
                    // set attribute to Parc Eolien element (Pour le id)
                    Attr attr = doc.createAttribute("name");
                    attr.setValue((nomParc) );
                    parcEolien.setAttributeNode(attr);
                    
                    // nom du parc
                    Element nomDuParc = doc.createElement("NomParc");
                    nomDuParc.appendChild(doc.createTextNode(nomParc ));
                    parcEolien.appendChild(nomDuParc);
                    
                    //orientation relative
                    Element x1_orientation = doc.createElement("x1_orientation");
                    x1_orientation.appendChild(doc.createTextNode(Integer.toString(orientation.getX1())));
                    parcEolien.appendChild(x1_orientation);
                    
                    Element y1_orientation = doc.createElement("y1_orientation");
                    y1_orientation.appendChild(doc.createTextNode(Integer.toString(orientation.getY1())));
                    parcEolien.appendChild(y1_orientation);
                    
                    Element x2_orientation = doc.createElement("x2_orientation");
                    x2_orientation.appendChild(doc.createTextNode(Integer.toString(orientation.getX2())));
                    parcEolien.appendChild(x2_orientation);
                    
                    Element y2_orientation = doc.createElement("y2_orientation");
                    y2_orientation.appendChild(doc.createTextNode(Integer.toString(orientation.getY2())));
                    parcEolien.appendChild(y2_orientation);
                    
                    // path Image
                    Element imageParc = doc.createElement("path_image");
                    imageParc.appendChild(doc.createTextNode(pathImage ));
                    parcEolien.appendChild(imageParc);
                    
                    //nom de l'operateur du parc Eolien
                    Element nom_operateur = doc.createElement("NomOperateur");
                    nom_operateur.appendChild(doc.createTextNode(nomOperateur ));
                    parcEolien.appendChild(nom_operateur);
                    
                    
                    //nombre d'�oliennes
                    Element nb_eoliennes = doc.createElement("NombreEoliennes");
                    nb_eoliennes.appendChild(doc.createTextNode(Integer.toString(listeEoliennes.size()) ));
                    parcEolien.appendChild(nb_eoliennes);
                    
                    /////////////////////////////////////////////////////////////////////
                    //Eoliennes//////////////////////////////////////////////////////////
                    ////////////////////////////////////////////////////////////////////
                    for (int i=0;i<listeEoliennes.size();i++){
                        Element eolienne = doc.createElement("Eolienne");
                        parcEolien.appendChild(eolienne);
                        
                        // set attribute to Eolienne element (Pour le id)
                        attr = doc.createAttribute("id");
                        attr.setValue(Integer.toString(i+1 ) );
                        eolienne.setAttributeNode(attr);
                        
                        // nomEolienne elements
                        Element nom_eolienne = doc.createElement("Nom");
                        nom_eolienne.appendChild(doc.createTextNode(listeEoliennes.get(i).getNomEolienne() ));
                        eolienne.appendChild(nom_eolienne);
                        
                        // nomModele elements
                        Element nom_modele = doc.createElement("Modele");
                        nom_modele.appendChild(doc.createTextNode(listeEoliennes.get(i).getModele() ));
                        eolienne.appendChild(nom_modele);
                        
                        // position X elements
                        Element pos_x = doc.createElement("position_X");
                        pos_x.appendChild(doc.createTextNode(Integer.toString(listeEoliennes.get(i).getPosX()) ));
                        eolienne.appendChild(pos_x);
                        
                        // position Y elements
                        Element pos_y = doc.createElement("position_Y");
                        pos_y.appendChild(doc.createTextNode(Integer.toString(listeEoliennes.get(i).getPosY()) ));
                        eolienne.appendChild(pos_y);
                        
                        // Largeur Eolienne
                        Element largeur = doc.createElement("largeur");
                        largeur.appendChild(doc.createTextNode(Integer.toString(listeEoliennes.get(i).getLargeur() ) ));
                        eolienne.appendChild(largeur);
                        
                        // hauteur Eolienne
                        Element hauteur = doc.createElement("hauteur");
                        hauteur.appendChild(doc.createTextNode(Integer.toString(listeEoliennes.get(i).getHauteur() ) ));
                        eolienne.appendChild(hauteur);
                        
                        //CHEMIN D'ACCES DE L'EOLIENNE
                        Element chemin_acces = doc.createElement("Chemin_Acces");
                        eolienne.appendChild(chemin_acces);
                        
                        Element pos_x1_chemin = doc.createElement("position_X1");
                        pos_x1_chemin.appendChild(doc.createTextNode(Integer.toString(h.get(listeEoliennes.get(i).getNomEolienne() ).getX1())  ));
                        chemin_acces.appendChild(pos_x1_chemin);
                        
                        Element pos_y1_chemin = doc.createElement("position_Y1");
                        pos_y1_chemin.appendChild(doc.createTextNode(Integer.toString(h.get(listeEoliennes.get(i).getNomEolienne() ).getY1() )  ));
                        chemin_acces.appendChild(pos_y1_chemin);
                        
                        Element pos_x2_chemin = doc.createElement("position_X2");
                        pos_x2_chemin.appendChild(doc.createTextNode(Integer.toString(h.get(listeEoliennes.get(i).getNomEolienne() ).getX2() )  ));
                        chemin_acces.appendChild(pos_x2_chemin);
                        
                        Element pos_y2_chemin = doc.createElement("position_Y2");
                        pos_y2_chemin.appendChild(doc.createTextNode(Integer.toString(h.get(listeEoliennes.get(i).getNomEolienne() ).getY2() )  ));
                        chemin_acces.appendChild(pos_y2_chemin);
                        
                        
                    }
                    /////////////////////////////////////////////////////////////////////////////////////////
                    
                    // write the content into xml file
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = transformerFactory.newTransformer();
                    DOMSource source = new DOMSource(doc);
                    StreamResult result = new StreamResult(fichierBD );

                    transformer.transform(source, result);
                    System.out.println("File saved!");
                    
                    //cr�ation de l'object PARC
                    comboParcs.addItem(nomParc);
                    comboParcs.setSelectedItem(nomParc);
                    parc = new Parc(nomParc, nomOperateur, listeEoliennes, listeChemins, pathImage, listeOrientation );
                    
                    listeParcs.put(nomParc, parc);                
                    
                    boutonEnregistrer.setEnabled(false);
                    
                    JOptionPane.showMessageDialog(FrameEdit.this, "Le parc a bien �t� sauvegard�");
                    listeEoliennes=null;
                    listeChemins=null;
                    listeOrientation=null;
                    listeEoliennes = new Vector<Eolienne>();
                    listeChemins = new Vector<CheminAcces>();
                    listeOrientation = new Vector<Orientation>();
                    
                }
               
                catch(TransformerException tfe){                                                            
                    tfe.printStackTrace();
                }
                catch(NullPointerException npe){
                    JOptionPane.showMessageDialog(FrameEdit.this, "Vous devez dessiner au moins 1 �olienne et l'orientation du parc");
                    supprimerParc(doc.getFirstChild(),nomParc);
                }
                
                
                
            }//fin du bouton Enregistrer
            
            repaint();
        }

    }
    
    private class EcouteurSurface implements MouseListener, MouseMotionListener, MouseWheelListener {
        @Override
        public void mouseClicked(MouseEvent e) {
            
            //SUPPRIMER UNE EOLIENNE
            if (boutonSupprimerEol.isSelected() ){
                Eolienne eol = selectionnerEolienne(e.getX(), e.getY());
                if(parcExiste() ){
                    if(eol != null){
                        int reponse = JOptionPane.showConfirmDialog(FrameEdit.this, "Etes-vous certain de vouloir supprimer cette �olienne?");
                        if (reponse == JOptionPane.YES_OPTION){
                            Node parcNode = trouverParc(doc.getFirstChild(), "Parc_Eolien");
                            supprimerEolienne(parcNode, "Eolienne", eol.getNomEolienne() );
                            commit();
                            
                            listeParcs.get(comboParcs.getSelectedItem().toString() ).getListeEoliennes().removeElement(eol);
                            listeParcs.get(comboParcs.getSelectedItem().toString()).getListeChemins().removeElement(h.get(eol.getNomEolienne()) );
                            xEolienne = -10000;
                            yEolienne = -10000;
                            x1chemin = y1chemin = x2chemin = y2chemin = -10000;
                        }
                          
                    }
                }
                else{ // on supprime les �l�ments qui sont temporaires.
                    
                    if(eol != null){
                        int reponse = JOptionPane.showConfirmDialog(FrameEdit.this, "Etes-vous certain de vouloir supprimer cette �olienne?");
                        if (reponse == JOptionPane.YES_OPTION){
                            listeEoliennes.removeElement(eol);
                            listeChemins.removeElement(h.get(eol.getNomEolienne()));
                            xEolienne = -10000;
                            yEolienne = -10000;
                            x1chemin = y1chemin = x2chemin = y2chemin = -10000;
                            
                        }
                          
                    }
                    
                }
                buttonGroupe.clearSelection();
                scrollImage = true;
                
                repaint();
                
            }
            else if(etatDessinerChemin){
                
                CheminAcces cheminAcces = new CheminAcces(nomEolienne,x1chemin-transX,y1chemin-transY,x2chemin-transX,y2chemin-transY);
                if(!parcExiste()){ //le parc existe pas
                    listeChemins.add(cheminAcces);
                    h.put(nomEolienne, cheminAcces);    
                }
                else{ // le parc existe
                    listeParcs.get(comboParcs.getSelectedItem().toString()).getListeChemins().add(cheminAcces);
                    h.put(nomEolienne, cheminAcces);
                    ajouterCheminBD(nomEolienne,cheminAcces);
                    commit();
                }
                
                
                scrollImage=true;
                etatDessinerChemin = false;
                
                repaint();
            }
        
        
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if(scrollImage ){
                Eolienne eol = selectionnerEolienne(e.getX(), e.getY());
                if(eol == null){
                    xImageDepart = e.getX();
                    yImageDepart = e.getY();
                    //xEolDepart = listeParcs.get(comboParcs.getSelectedItem().toString()).getListeEoliennes()
                    xEolienne = -10000;
                    yEolienne = -10000;
                    x1chemin = y1chemin = x2chemin = y2chemin = -10000;
                    x1orientation = y1orientation = x2orientation = y2orientation = -10000;
                }
                else{
                    champNomSelection.setText(eol.getNomEolienne() );
                    champModele.setText(eol.getModele() );
                    champLargeur.setText( Integer.toString(eol.getLargeur()) );
                    champHauteur.setText( Integer.toString(eol.getHauteur()) );
                }
                

            }
           
            
            else if(boutonAjouterEolienne.isSelected() ){
                xEolienne = e.getX();
                yEolienne = e.getY();
                
                repaint();
                
            }
            
            else if(boutonOrientation.isSelected() ){
                x1orientation = e.getX();
                y1orientation = e.getY();
                x2orientation = e.getX();
                y2orientation = e.getY();
            }
            
            
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if(scrollImage ){
                transX += xImage-xImageDepart;
                transY += yImage-yImageDepart ;
                
                xImage=0;
                xImageDepart=0;
                yImage=0;
                yImageDepart=0;
            }
            
            else if(boutonAjouterEolienne.isSelected() ){
                    Eolienne eolienne = new Eolienne(nomEolienne, modeleEolienne, xEolienne - transX, yEolienne - transY, largeurEolienne, hauteurEolienne );
                    
                    if (!parcExiste() ){ //parc pas encore cr��
                        listeEoliennes.add(eolienne);
                    }
                    else{ //le parc est cr��, on ajoute l'�olienne dans la BD
                        listeParcs.get(comboParcs.getSelectedItem().toString()).getListeEoliennes().add(eolienne);
                        ajouterEolienneBD(eolienne);
                        commit();
                    }
                    ///settings pour commencer a dessiner le chemin d'acces
                    x1chemin = e.getX();
                    y1chemin = e.getY();
                    x2chemin = e.getX();
                    y2chemin = e.getY();
                    
                    etatDessinerChemin = true;
                    
                    
                    buttonGroupe.clearSelection();
                    
            }
            else if (boutonOrientation.isSelected() ){
                
                orientation = new Orientation(x1orientation,y1orientation,x2orientation,y2orientation );
                listeOrientation.add(orientation);
                buttonGroupe.clearSelection();
                scrollImage = true;
            }
            
            repaint();
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

        @Override
        public void mouseDragged(MouseEvent e)  {
            if(scrollImage ){
                xImage = e.getX();
                yImage = e.getY();
                
            }
            
            if(boutonAjouterEolienne.isSelected()  ){
                xEolienne = e.getX();
                yEolienne = e.getY();
            }
            
            else if (boutonOrientation.isSelected() ){
                x2orientation = e.getX();
                y2orientation = e.getY();
            }
            
            
            repaint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            if(etatDessinerChemin){
                x2chemin = e.getX();
                y2chemin = e.getY();
            }
            repaint();
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            if(zoom >= 0) {
                zoom -=  0.05f * e.getWheelRotation();
                System.out.println(zoom);
                
                repaint();
            }
            else{
                zoom=0;
            }
            
        }

    }
    
    //////////////////////////////////////////////////
    //Map
    /////////////////////////////////////////////////
    private class SurfaceDessin extends JPanel{
        protected void paintComponent (Graphics g){
              
          super.paintComponent(g);
    
          Graphics2D g2D = (Graphics2D) g;
          if(image != null){
            AffineTransform t = new AffineTransform();
            float currentImgWidth = image.getWidth(null)*zoom;
            float currentImgHeight = image.getHeight(null)*zoom;
            t.translate(xImage-xImageDepart + transX,yImage-yImageDepart + transY);
            
            
            t.scale(zoom, zoom);
            //Et j'affiche en utilisant la transformation
            g2D.drawImage(image, t, null);
            
          }
          
          //////////////////////
          //DESSIN COURANT/////
          //////////////////////  
            if(listeEoliennes != null){ //DESSIN DES EOLIENNES
                g2D.setColor(Color.red);
                g2D.setStroke(new BasicStroke(largeurEolienne));
                g2D.drawLine(xEolienne, yEolienne, xEolienne, yEolienne);
                      
            }
            if(etatDessinerChemin){ //DESSIN DES CHEMINS D'ACCES
                g2D.setColor(Color.yellow);
                g2D.setStroke(new BasicStroke(3));
                g2D.drawLine(x1chemin, y1chemin, x2chemin, y2chemin);
            }
            if(listeOrientation != null ){
                g2D.setColor(Color.blue);
                g2D.setStroke(new BasicStroke(3));
                g2D.drawLine(x1orientation, y1orientation, x2orientation, y2orientation);
                g2D.fillOval(x2orientation-8, y2orientation-8, 16, 16);
                      
            }
                  
            /////////////////////////////////////
            //DESSINER les eoliennes et les chemin d'acces en fonction du parc s�lectionn�
            /////////////////////////////////////
            String selectionParc;
            if(comboParcs.getSelectedItem() != null){
                selectionParc = comboParcs.getSelectedItem().toString();
            }
            else{
                selectionParc = nomParc;
            }
            //le parc selectionn� existe, donc on dessine les objects conserv�
            if (listeParcs.size() != 0){
                if(listeParcs.get(selectionParc) != null){
                    //DESSIN DES EOLIENNES
                    for (int i=0;i<listeParcs.get(selectionParc).getListeEoliennes().size();i++){
                        g2D.setColor(Color.red);
                        g2D.setStroke(new BasicStroke(listeParcs.get(selectionParc).getListeEoliennes().get(i).getLargeur()));
                        int posX = listeParcs.get(selectionParc).getListeEoliennes().get(i).getPosX();
                        int posY = listeParcs.get(selectionParc).getListeEoliennes().get(i).getPosY();
                       // g2D.drawLine(posX-xImageDepart+transX, posY-yImageDepart+transY, posX-xImageDepart+transX, posY-yImageDepart+transY );
                        g2D.drawLine(posX+(xImage-xImageDepart)+transX, posY+yImage-yImageDepart+transY, posX+xImage-xImageDepart+transX, posY+yImage-yImageDepart+transY );
                        
                        
                    }
                    //DESSIN DES CHEMINS D'ACCES
                    for (int i=0;i<listeParcs.get(selectionParc).getListeChemins().size();i++ ){
                        g2D.setColor(Color.yellow);
                        g2D.setStroke(new BasicStroke(3) );
                        int posX1 = listeParcs.get(selectionParc).getListeChemins().get(i).getX1();
                        int posY1 = listeParcs.get(selectionParc).getListeChemins().get(i).getY1();
                        int posX2 = listeParcs.get(selectionParc).getListeChemins().get(i).getX2();
                        int posY2 = listeParcs.get(selectionParc).getListeChemins().get(i).getY2();
                        g2D.drawLine(posX1+(xImage-xImageDepart)+transX,posY1+(yImage-yImageDepart)+transY,posX2+(xImage-xImageDepart)+transX,posY2+(yImage-yImageDepart)+transY);
                    }
                    //DESSIN DE L'ORIENTATION RELATIVE
                    for(int i=0;i<listeParcs.get(selectionParc).getListeOrientation().size();i++ ){
                        g2D.setColor(Color.blue);
                        g2D.setStroke(new BasicStroke(3));
                        int x1 = listeParcs.get(selectionParc).getListeOrientation().get(i).getX1();
                        int y1 = listeParcs.get(selectionParc).getListeOrientation().get(i).getY1();
                        int x2 = listeParcs.get(selectionParc).getListeOrientation().get(i).getX2();
                        int y2 = listeParcs.get(selectionParc).getListeOrientation().get(i).getY2();
                        g2D.drawLine(x1, y1, x2, y2);
                        g2D.fillOval(x2-8, y2-8, 16, 16);
                    }
                    
                    
                }
                    
                //le parc s�lectionn� n'existe pas, donc on dessine les objects temporaire
                else{
                    if(listeEoliennes.size() != 0){
                        for (int i=0;i<listeEoliennes.size();i++){
                            g2D.setColor(Color.red);
                            g2D.setStroke(new BasicStroke(listeEoliennes.get(i).getLargeur()));
                            int posX = listeEoliennes.get(i).getPosX();
                            int posY = listeEoliennes.get(i).getPosY();
                            g2D.drawLine(posX+(xImage-xImageDepart)+transX,posY+(yImage-yImageDepart)+transY,posX+(xImage-xImageDepart)+transX,posY+(yImage-yImageDepart)+transY);
                            
                        }
                        
                    }
                    if (listeChemins.size() != 0){
                        for(int i=0;i<listeChemins.size();i++){
                            g2D.setColor(Color.yellow);
                            g2D.setStroke(new BasicStroke(3) );
                            int posX1 = listeChemins.get(i).getX1();
                            int posY1 = listeChemins.get(i).getY1();
                            int posX2 = listeChemins.get(i).getX2();
                            int posY2 = listeChemins.get(i).getY2();
                            
                            g2D.drawLine(posX1+(xImage-xImageDepart)+transX,posY1+(yImage-yImageDepart)+transY,posX2+(xImage-xImageDepart)+transX,posY2+(yImage-yImageDepart)+transY);
                        }
                        
                    }
                    if(listeOrientation.size() != 0){
                        //DESSIN DE L'ORIENTATION RELATIVE
                        for(int i=0;i<listeOrientation.size();i++){
                            g2D.setColor(Color.blue);
                            g2D.setStroke(new BasicStroke(3));
                            int x1 = listeOrientation.get(i).getX1();
                            int y1 = listeOrientation.get(i).getY1();
                            int x2 = listeOrientation.get(i).getX2();
                            int y2 = listeOrientation.get(i).getY2();
                            g2D.drawLine(x1, y1, x2, y2);
                            g2D.fillOval(x2-8, y2-8, 16, 16);
                        }
                        
                    }
                    
                }
                 
            }
            //aucun parc de cr�� encore
            else{
                if(listeEoliennes != null) {
                    for (int i=0;i<listeEoliennes.size();i++){
                        g2D.setColor(Color.red);
                        g2D.setStroke(new BasicStroke(listeEoliennes.get(i).getLargeur()));
                        int posX = listeEoliennes.get(i).getPosX();
                        int posY = listeEoliennes.get(i).getPosY();
                        g2D.drawLine(posX+(xImage-xImageDepart)+transX,posY+(yImage-yImageDepart)+transY,posX+(xImage-xImageDepart)+transX,posY+(yImage-yImageDepart)+transY);
                    
                    }
                }
                if(listeChemins != null){
                    for(int i=0;i<listeChemins.size();i++){
                        g2D.setColor(Color.yellow);
                        g2D.setStroke(new BasicStroke(3) );
                        int posX1 = listeChemins.get(i).getX1();
                        int posY1 = listeChemins.get(i).getY1();
                        int posX2 = listeChemins.get(i).getX2();
                        int posY2 = listeChemins.get(i).getY2();
                        
                        g2D.drawLine(posX1+(xImage-xImageDepart)+transX,posY1+(yImage-yImageDepart)+transY,posX2+(xImage-xImageDepart)+transX,posY2+(yImage-yImageDepart)+transY);
                    }
                    
                }
                if(listeOrientation != null){
                    //DESSIN DE L'ORIENTATION RELATIVE
                    for(int i=0;i<listeOrientation.size();i++){
                        g2D.setColor(Color.blue);
                        g2D.setStroke(new BasicStroke(3));
                        int x1 = listeOrientation.get(i).getX1();
                        int y1 = listeOrientation.get(i).getY1();
                        int x2 = listeOrientation.get(i).getX2();
                        int y2 = listeOrientation.get(i).getY2();
                        g2D.drawLine(x1, y1, x2, y2);
                        g2D.fillOval(x2-8, y2-8, 16, 16);
                    }
                    
                }
            }
            
        }//fin du paintComponent

    }
    
    public void recupererInfosDialogueEolienne(String nom, String modele, int largeur, int hauteur){
        this.nomEolienne=nom;
        this.modeleEolienne=modele;
        this.largeurEolienne=largeur;
        this.hauteurEolienne=hauteur;
    }
    public void recupererInfosDialogueNouveauParc(String nomParc,String nomOperateur,BufferedImage imageParc, String pathImage){
        this.nomParc=nomParc;
        this.nomOperateur=nomOperateur;
        this.image=imageParc;
        this.largeurImage = imageParc.getWidth();
        this.hauteurImage = imageParc.getHeight();
        this.pathImage = pathImage;
        repaint();
    }
    
    public void setD(DialogNouveauParc d) {
        this.d = d;
    }

    public DialogNouveauParc getD() {
        return d;
    }

    public void setBoutonEnregistrer(JButton boutonEnregistrer) {
        this.boutonEnregistrer = boutonEnregistrer;
    }

    public JButton getBoutonEnregistrer() {
        return boutonEnregistrer;
    }
    public void setMenuNouveauParc(JMenuItem menuNouveauParc) {
        this.menuNouveauParc = menuNouveauParc;
    }

    public JMenuItem getMenuNouveauParc() {
        return menuNouveauParc;
    }

    public void setNomParc(String nomParc) {
        this.nomParc = nomParc;
    }

    public String getNomParc() {
        return nomParc;
    }
    public Vector<Eolienne> getListeEoliennes() {
        return listeEoliennes;
    }
    public void setLabelNomParc(JLabel labelNomParc) {
        this.labelNomParc = labelNomParc;
    }

    public JLabel getLabelNomParc() {
        return labelNomParc;
    }
    public void setComboParcs(JComboBox comboParcs) {
        this.comboParcs = comboParcs;
    }

    public JComboBox getComboParcs() {
        return comboParcs;
    }
    public FrameEdit.Ecouteur getEc() {
        return ec;
    }

    public JTextField getChampParcEnCours() {
        return champParcEnCours;
    }
    
    public Eolienne selectionnerEolienne(int x,int y){
        String SelectionCombo;
        if(comboParcs.getSelectedItem() != null){
            SelectionCombo = comboParcs.getSelectedItem().toString();
        }
        else{
            SelectionCombo = nomParc;
        }
        
        if(parcExiste()){ //le parc existe
            for(int i=0;i<listeParcs.get(SelectionCombo).getListeEoliennes().size();i++ ){
                //gestion du clic sur la bonne eolienne en fonction de sa largeur
                double moitieLargeur = listeParcs.get(SelectionCombo).getListeEoliennes().get(i).getLargeur()/2.0;
                if(x <= listeParcs.get(SelectionCombo).getListeEoliennes().get(i).getPosX()+ moitieLargeur + transX)
                    if(x >= listeParcs.get(SelectionCombo).getListeEoliennes().get(i).getPosX()-moitieLargeur + transX)
                        if(y >= listeParcs.get(SelectionCombo).getListeEoliennes().get(i).getPosY()-moitieLargeur + transY)
                            if(y <= listeParcs.get(SelectionCombo).getListeEoliennes().get(i).getPosY()+moitieLargeur + transY){
                                return listeParcs.get(SelectionCombo).getListeEoliennes().get(i);
                            }
                    
            }
        }
        else{  // le parc existe pas
            for(int i=0;i<listeEoliennes.size();i++ ){
                //gestion du clic sur la bonne eolienne en fonction de sa largeur
                double moitieLargeur = listeEoliennes.get(i).getLargeur()/2.0;
                if(x <= listeEoliennes.get(i).getPosX()+ moitieLargeur + transX)
                    if(x >= listeEoliennes.get(i).getPosX()-moitieLargeur + transX)
                        if(y >= listeEoliennes.get(i).getPosY()-moitieLargeur + transY)
                            if(y <= listeEoliennes.get(i).getPosY()+moitieLargeur + transY){
                                return listeEoliennes.get(i);
                            }
                    
            }
        }
        return null;
    }
    public void commit(){
        try{
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(fichierBD );

            transformer.transform(source, result);
            System.out.println("File saved!");
        }
        catch(TransformerException tfe){
            tfe.printStackTrace();
        }
    }
    public void supprimerParc(Node parent, String parcAsupprimer){
        NodeList children = parent.getChildNodes();  
        System.out.println("SIZE : " + children.getLength());
        for( int i=0; i < children.getLength(); i++ ){  
            Node child = children.item( i );  
               
            // only interested in elements  
            if( child.getNodeType() == Node.ELEMENT_NODE ){  
               
                // remove elements whose tag name  = filter  
                // otherwise check its children for filtering with a recursive call  
                if( child.getNodeName().equals("Parc_Eolien") ){
                    NodeList nodesDuParc = child.getChildNodes();
                    for (int j=0;j<nodesDuParc.getLength();j++){
                        Element childParc = (Element) nodesDuParc.item(j);
                        if( childParc.getNodeType() == Node.ELEMENT_NODE ){
                            if(childParc.getTagName().equals("NomParc") ){
                                if(childParc.getTextContent().equals(parcAsupprimer ) ){
                                    parent.removeChild( child );
                                }
                            }
                        }
                        
                    }
                    
                    
                } 
                else {  
                    supprimerParc( child, "Parc_Eolien" );  
                }  
            }  
        }  
    }
    public Node trouverParc(Node parent, String filter){
        NodeList children = parent.getChildNodes();  
        System.out.println("SIZE : " + children.getLength());
        for( int i=0; i < children.getLength(); i++ ){  
            Node child = children.item( i );  
               
            // only interested in elements  
            if( child.getNodeType() == Node.ELEMENT_NODE ){  
               
                // remove elements whose tag name  = filter  
                // otherwise check its children for filtering with a recursive call  
                if( child.getNodeName().equals(filter) ){
                    NodeList nodesDuParc = child.getChildNodes();
                    for (int j=0;j<nodesDuParc.getLength();j++){
                        Element childParc = (Element) nodesDuParc.item(j);
                        if( childParc.getNodeType() == Node.ELEMENT_NODE ){
                            if(childParc.getTagName().equals("NomParc") ){
                                if(childParc.getTextContent().equals(comboParcs.getSelectedItem().toString() ) ){
                                    return child;
                                }
                            }
                        }
                        
                    }
                    
                    
                } 
            }  
        }  
        return null;
    }
    
    public Node trouverEolienne(String nomEol){
        Node parcNode = trouverParc(doc.getFirstChild(), "Parc_Eolien");
         
        NodeList children = parcNode.getChildNodes();  
        for( int i=0; i < children.getLength(); i++ ){  
            Node child = children.item( i );  
            // only interested in elements  
            if( child.getNodeType() == Node.ELEMENT_NODE ){  
                if( child.getNodeName().equals("Eolienne") ){
                    NodeList nodesEolienne = child.getChildNodes();
                    for (int j=0;j<nodesEolienne.getLength();j++){
                        Element chilEolienne = (Element) nodesEolienne.item(j);
                        if( chilEolienne.getNodeType() == Node.ELEMENT_NODE ){
                            if(chilEolienne.getTagName().equals("Nom") ){
                                if(chilEolienne.getTextContent().equals(nomEol ) ){
                                    return child;
                                }
                            }
                        }
                        
                    }
                    
                    
                } 
            }  
        }
        return null;
    }
    
    
    public void supprimerEolienne(Node parent, String filter, String nomEolienne){
        NodeList children = parent.getChildNodes();  
        System.out.println("SIZE : " + children.getLength());
        for( int i=0; i < children.getLength(); i++ ){  
            Node child = children.item( i );  
               
            // only interested in elements  
            if( child.getNodeType() == Node.ELEMENT_NODE ){  
               
                // remove elements whose tag name  = filter  
                // otherwise check its children for filtering with a recursive call  
                if( child.getNodeName().equals(filter) ){
                    NodeList nodesDeEolienne = child.getChildNodes();
                    for (int j=0;j<nodesDeEolienne.getLength();j++){
                        Element childEolienne = (Element) nodesDeEolienne.item(j);
                        if( childEolienne.getNodeType() == Node.ELEMENT_NODE ){
                            if(childEolienne.getTagName().equals("Nom") ){
                                if(childEolienne.getTextContent().equals(nomEolienne ) ){
                                    parent.removeChild( child );
                                }
                            }
                        }
                        
                    }
                    
                    
                } 
                else {  
                    supprimerEolienne( child, filter, nomEolienne );  
                }  
            }  
        }  
    }
    
    public void ajouterCheminBD(String nomEolienne ,CheminAcces chemin){
        Node eolienneNode = trouverEolienne(nomEolienne);
        
        Element chemin_acces = doc.createElement("Chemin_Acces");
        eolienneNode.appendChild(chemin_acces);
        
        Element pos_x1_chemin = doc.createElement("position_X1");
        pos_x1_chemin.appendChild(doc.createTextNode(Integer.toString(chemin.getX1()) ) );
        chemin_acces.appendChild(pos_x1_chemin);
        
        Element pos_y1_chemin = doc.createElement("position_Y1");
        pos_y1_chemin.appendChild(doc.createTextNode(Integer.toString(chemin.getY1() )  ));
        chemin_acces.appendChild(pos_y1_chemin);
        
        Element pos_x2_chemin = doc.createElement("position_X2");
        pos_x2_chemin.appendChild(doc.createTextNode(Integer.toString(chemin.getX2() )  ));
        chemin_acces.appendChild(pos_x2_chemin);
        
        Element pos_y2_chemin = doc.createElement("position_Y2");
        pos_y2_chemin.appendChild(doc.createTextNode(Integer.toString(chemin.getY2() )  ));
        chemin_acces.appendChild(pos_y2_chemin);
    }
    
    
    public void ajouterEolienneBD(Eolienne eol){
        Element parcEolien = (Element) trouverParc(doc.getFirstChild(), "Parc_Eolien" );
        System.out.println("ELEMENT PARCEOLIEN : " + parcEolien);
        
        Element eolienne = doc.createElement("Eolienne");
        parcEolien.appendChild(eolienne);
        // set attribute to Eolienne element (Pour le id)
        Attr attr = doc.createAttribute("name");
        attr.setValue(eol.getNomEolienne() );
        eolienne.setAttributeNode(attr);
        
        // nomEolienne elements
        Element nom_eolienne = doc.createElement("Nom");
        nom_eolienne.appendChild(doc.createTextNode(eol.getNomEolienne() ));
        eolienne.appendChild(nom_eolienne);
        
        // nomModele elements
        Element nom_modele = doc.createElement("Modele");
        nom_modele.appendChild(doc.createTextNode(eol.getModele() ));
        eolienne.appendChild(nom_modele);
        
        // position X elements
        Element pos_x = doc.createElement("position_X");
        pos_x.appendChild(doc.createTextNode(Integer.toString(eol.getPosX()) ));
        eolienne.appendChild(pos_x);
        
        // position Y elements
        Element pos_y = doc.createElement("position_Y");
        pos_y.appendChild(doc.createTextNode(Integer.toString(eol.getPosY()) ));
        eolienne.appendChild(pos_y);
        
        // Largeur Eolienne
        Element largeur = doc.createElement("largeur");
        largeur.appendChild(doc.createTextNode(Integer.toString(eol.getLargeur() ) ));
        eolienne.appendChild(largeur);
        
        // hauteur Eolienne
        Element hauteur = doc.createElement("hauteur");
        hauteur.appendChild(doc.createTextNode(Integer.toString(eol.getHauteur() ) ));
        eolienne.appendChild(hauteur);
    }
    public boolean parcExiste(){
        String SelectionCombo;
        if(comboParcs.getSelectedItem() != null){
            SelectionCombo = comboParcs.getSelectedItem().toString();
        }
        else{
            SelectionCombo = nomParc;
        }
        
        if(listeParcs.size() != 0){
            if(listeParcs.get(SelectionCombo) != null){ // le parc existe
                return true;
            }
        }
        return false;          
   
    }
    public void chargerBd(){
        try {
                listeEoliennes = new Vector<Eolienne>();
                listeChemins = new Vector<CheminAcces>();
                listeParcs = new Hashtable<String,Parc>();
                h = new Hashtable<String,CheminAcces>();
            
                File fXmlFile = new File("BD.xml");
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                doc = dBuilder.parse(fXmlFile);

                //optional, but recommended
                //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
                doc.getDocumentElement().normalize();

                NodeList nodeListeParcs = doc.getElementsByTagName("Parc_Eolien");
                for (int i=0;i<nodeListeParcs.getLength();i++){
                    //titre Parc
                    Node nNode = nodeListeParcs.item(i);
                                                
                    System.out.println("\nCurrent Element :" + nNode.getNodeName());
                    Element eElement = null;
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        eElement = (Element) nNode;
                        nomParc = eElement.getElementsByTagName("NomParc").item(0).getTextContent();
                        nomOperateur = eElement.getElementsByTagName("NomOperateur").item(0).getTextContent();
                        pathImage = eElement.getElementsByTagName("path_image").item(0).getTextContent();
                        x1orientation = Integer.parseInt(eElement.getElementsByTagName("x1_orientation").item(0).getTextContent()) ;
                        y1orientation = Integer.parseInt(eElement.getElementsByTagName("y1_orientation").item(0).getTextContent()) ;
                        x2orientation = Integer.parseInt(eElement.getElementsByTagName("x2_orientation").item(0).getTextContent()) ;
                        y2orientation = Integer.parseInt(eElement.getElementsByTagName("y2_orientation").item(0).getTextContent()) ;
                        
                        comboParcs.addItem(nomParc);
                        champOperateur.setText(nomOperateur);
                        System.out.println("NomParc: " + eElement.getElementsByTagName("NomParc").item(0).getTextContent());
                        System.out.println("NomOperateur: " + eElement.getElementsByTagName("NomOperateur").item(0).getTextContent());
                        System.out.println("NombreEoliennes: " + eElement.getElementsByTagName("NombreEoliennes").item(0).getTextContent());          
                                                
                    }
                       
                        //LES ENFANTS DU PARC
                        NodeList nodes = eElement.getChildNodes();
                        for (int j=0;j<nodes.getLength();j++){
                            Element child = (Element) nodes.item(j);
                            if (child.getNodeType() == Node.ELEMENT_NODE){
                                if (child.getTagName().equals("Eolienne") ){
                                    NodeList nodeListeEoliennes = child.getChildNodes();
                                        
                                    for (int k=0;k<nodeListeEoliennes.getLength();k++){
                                        System.out.println("K : "+k);
                                        Element eolienneChild = (Element) nodeListeEoliennes.item(k);
                                        if(eolienneChild.getNodeType() == Element.ELEMENT_NODE){
                                            if(eolienneChild.getTagName().equals("Nom") ){
                                                nomEolienne = eolienneChild.getTextContent();
                                                System.out.println(eolienneChild.getTagName() + " : " +nomEolienne);
                                            }
                                            else if(eolienneChild.getTagName().equals("Modele") ){
                                                modeleEolienne = eolienneChild.getTextContent();
                                                System.out.println(eolienneChild.getTagName() + " : " +modeleEolienne);
                                            }
                                            else if(eolienneChild.getTagName().equals("position_X") ){
                                                xEolienne = Integer.parseInt(eolienneChild.getTextContent() ) ;
                                                System.out.println(eolienneChild.getTagName() + " : " +xEolienne);
                                            }
                                            else if(eolienneChild.getTagName().equals("position_Y") ){
                                                yEolienne = Integer.parseInt(eolienneChild.getTextContent() ) ;
                                                System.out.println(eolienneChild.getTagName() + " : " +yEolienne);
                                            }
                                            else if(eolienneChild.getTagName().equals("largeur") ){
                                                largeurEolienne = Integer.parseInt(eolienneChild.getTextContent() ) ;
                                                System.out.println(eolienneChild.getTagName() + " : " +largeurEolienne);
                                            }
                                            else if(eolienneChild.getTagName().equals("hauteur") ){
                                                hauteurEolienne = Integer.parseInt(eolienneChild.getTextContent() ) ;
                                                System.out.println(eolienneChild.getTagName() + " : " +hauteurEolienne);
                                            }
                                            else{
                                                NodeList cheminChilds = eolienneChild.getChildNodes();
                                                for(int m = 0; m < cheminChilds.getLength(); m++){
                                                    Element cheminChild = (Element) cheminChilds.item(m);
                                                    if(cheminChild.getNodeType() == Element.ELEMENT_NODE){
                                                        if(cheminChild.getTagName().equals("position_X1") ){
                                                            x1chemin = Integer.parseInt(cheminChild.getTextContent() ) ;
                                                        }
                                                        else if(cheminChild.getTagName().equals("position_Y1") ){
                                                            y1chemin = Integer.parseInt(cheminChild.getTextContent() ) ;
                                                        }
                                                        else if(cheminChild.getTagName().equals("position_X2") ){
                                                            x2chemin = Integer.parseInt(cheminChild.getTextContent() ) ;
                                                        }
                                                        else if(cheminChild.getTagName().equals("position_Y2") ){
                                                            y2chemin = Integer.parseInt(cheminChild.getTextContent() ) ;
                                                        }
                                                        
                                                    }
                                                    
                                                }
                                                CheminAcces chemin = new CheminAcces(nomEolienne,x1chemin,y1chemin,x2chemin,y2chemin );
                                                listeChemins.add(chemin);
                                                h.put(nomEolienne, chemin);
                                                
                                            }
                                        }
                                        
                                        
                                    }
                                    System.out.println("EOLIENNE CR�E");                        
                                    Eolienne e = new Eolienne(nomEolienne, modeleEolienne, xEolienne, yEolienne,largeurEolienne,hauteurEolienne);
                                    listeEoliennes.add(e);
                                }
                            }
                        
                    
                    }
                    orientation = new Orientation(x1orientation,y1orientation,x2orientation,y2orientation);
                    listeOrientation.add(orientation);
                    Parc p = new Parc(nomParc, nomOperateur,listeEoliennes, listeChemins, pathImage, listeOrientation);
                    listeParcs.put(nomParc, p);
                    listeEoliennes = null;
                    listeChemins = null;
                    listeOrientation = null;
                    listeEoliennes = new Vector<Eolienne>();
                    listeChemins = new Vector<CheminAcces>();
                    listeOrientation = new Vector<Orientation>();
                    
                    
                }
                
        
        

            } 
            catch (Exception e) {
                e.printStackTrace();
            }
        xEolienne = -30;
        yEolienne = -30;
        x1chemin = -1000;
        x2chemin = -1000;
        y1chemin = -1000;
        y2chemin = -1000;
        x1orientation = -10000;
        y1orientation = -10000;
        x2orientation = -10000;
        y2orientation = -10000;
    }
    
    public void loaderImage(){
        if(comboParcs.getSelectedItem() != null ){
            String path = listeParcs.get(comboParcs.getSelectedItem().toString()).getPathImage();
            System.out.println("Chemin image : "+path);
            try {
                image = ImageIO.read(new File(path));
                
            } catch (IOException exc) {
                exc.printStackTrace();
            } 
        }
    }
    

}
