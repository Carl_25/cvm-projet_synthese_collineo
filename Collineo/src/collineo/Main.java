package collineo;

import javax.swing.JFrame;


public class Main {
    public static void main(String[] args) {
        FrameEdit f = new FrameEdit();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
}
