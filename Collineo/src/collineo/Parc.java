package collineo;

import java.util.Vector;

public class Parc {
    private String nomParc;
    private String nomOperateur;
    private Vector<Eolienne> listeEoliennes;
    private Vector<CheminAcces> listeChemins;
    private Vector<Orientation> listeOrientation;
    private String pathImage;
    private static int id=0;
    
    public Parc(String nomParc,String nomOperateur, Vector<Eolienne> listeEoliennes, Vector<CheminAcces> listeChemins, String pathImage, Vector<Orientation> listeOrientation){
        this.nomParc=nomParc;
        this.nomOperateur=nomOperateur;
        
        this.listeEoliennes = listeEoliennes;
        
        this.listeChemins=listeChemins;
        this.pathImage=pathImage;
        this.listeOrientation = listeOrientation;
        this.id++;
    }

    public String getNomParc() {
        return nomParc;
    }

    public String getNomOperateur() {
        return nomOperateur;
    }


    public static int getId() {
        return id;
    }

    public Vector<Eolienne> getListeEoliennes() {
        //System.out.println(listeEoliennes.get(0).getNomEolienne());
        return listeEoliennes;
    }

    public void setListeChemins(Vector<CheminAcces> listeChemins) {
        this.listeChemins = listeChemins;
    }

    public Vector<CheminAcces> getListeChemins() {
        return listeChemins;
    }

    public void setNomOperateur(String nomOperateur) {
        this.nomOperateur = nomOperateur;
    }

    public String getPathImage() {
        return pathImage;
    }

    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }

    public Vector<Orientation> getListeOrientation() {
        return listeOrientation;
    }
}
